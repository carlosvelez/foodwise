

########################################################################################################################
from django.test import TestCase
from foodwise.models import *


########################################################################################################################
class TestIngredient(TestCase):
    ####################################################################################################################
    def setUp(self):
        self.carrot = Ingredient.objects.create(name="carrot")
        self.zucchini = Ingredient.objects.create(name="zucchini")
        self.garlic = Ingredient.objects.create(name="garlic")
        self.eggs = Ingredient.objects.create(name="eggs")

    ####################################################################################################################
    def test_name(self):
        self.assertEqual(4, Ingredient.objects.count())
        carrot = Ingredient.objects.get(name="carrot")
        zucchini = Ingredient.objects.get(name="zucchini")
        garlic = Ingredient.objects.get(name="garlic")
        eggs = Ingredient.objects.get(name="eggs")
        self.assertEqual(self.carrot.name, carrot.name)
        self.assertEqual(self.zucchini.name, zucchini.name)
        self.assertEqual(self.garlic.name, garlic.name)
        self.assertEqual(self.eggs.name, eggs.name)


########################################################################################################################
class TestMeal(TestCase):
    ####################################################################################################################
    def setUp(self):
        self.meal = Meal.objects.create()
        self.carrot = self.meal.ingredients.create(name="carrot")
        self.zucchini = self.meal.ingredients.create(name="zucchini")
        self.garlic = self.meal.ingredients.create(name="garlic")
        self.eggs = self.meal.ingredients.create(name="eggs")
        self.meal.save()

    ####################################################################################################################
    def test_ingredients(self):
        num_ingredients = len(Ingredient.objects.filter(meal=self.meal))
        self.assertEqual(4, num_ingredients)
