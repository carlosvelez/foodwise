from django.conf.urls import url
from journal import views


########################################################################################################################
urlpatterns = [
    url(r'^$', views.journal, name="journal"),
    url(r'^entry$', views.journal_entry, name="journal_entry"),
]
