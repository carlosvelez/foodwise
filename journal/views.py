########################################################################################################################
from django.shortcuts import render
from django.http import HttpResponse
from journal.forms import MealEntryForm


########################################################################################################################
from journal.models import Meal


def journal(request):
    return HttpResponse("The FoodWise Food Journal.")


########################################################################################################################
def journal_entry(request):
    if request.method == "POST":
        form = MealEntryForm(request.POST, request.FILES)
        if form.is_valid():
            photo = form.cleaned_data["photo"]
            rotation = form.cleaned_data["rotation"]
            ingredients = form.cleaned_data["ingredients"]
            timestamp = form.cleaned_data["timestamp"]
            note = form.cleaned_data["note"]

            print "Photo created on: ", photo.created
            if rotation:
                print "Rotate photo ", rotation
            print "Ingredients: ", ingredients
            if timestamp:
                print "Timestamp:", timestamp
            if note:
                print "Note:", note
    else:
        form = MealEntryForm()

    return render(request, "journal/journal_entry.html", {
        "form": form,
    })

    # return render(request, 'admin/kobo_revised_epub_import.html', {
# +        "form": form,
# +        "form_errors": form_errors,
# +        "xls_file_upload": xls_file_upload,
# +        "upload_errors": upload_errors,
# +        "num_updated_isbns": num_updated_isbns,
# +        "upload_success": upload_success,
# +        "email_success": email_success,
# +    })