# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('foodwise', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='JournalEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Note', models.CharField(max_length=720, null=True, verbose_name=b'Notes', blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('timestamp', models.DateTimeField(default=django.utils.timezone.now, blank=True)),
                ('photo', models.ManyToManyField(to='foodwise.Photo', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='JournalEntryPhoto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Photo', models.ImageField(upload_to=b'', verbose_name=b'Photos', blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('timestamp_override', models.DateTimeField(default=None, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Meal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Note', models.CharField(max_length=720, null=True, verbose_name=b'Notes', blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('timestamp', models.DateTimeField(default=django.utils.timezone.now, null=True, blank=True)),
                ('ingredients', models.ManyToManyField(to='foodwise.Ingredient')),
                ('photo', models.ManyToManyField(to='foodwise.Photo', blank=True)),
            ],
        ),
    ]
