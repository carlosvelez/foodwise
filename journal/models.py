########################################################################################################################
from django.db import models
from django.utils import timezone
from foodwise.models import Ingredient, Photo


########################################################################################################################
class JournalEntryPhoto(models.Model):
    file = models.ImageField(name="Photo", verbose_name="Photos", blank=True)
    created = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    timestamp_override = models.DateTimeField(default=None, blank=True)

    @property
    ####################################################################################################################
    def timestamp(self):
        if self.timestamp_override:
            return self.timestamp_override
        else:
            return "{:%Y/%m/%d %H:%M".format(self.created)

    ####################################################################################################################
    def set_timestamp_from_photo(self):
        timestamp = None
        self.timestamp_override = timestamp
        self.timestamp_override.save()
        raise NotImplementedError("You have to figure out how to get a created datetime from an uploaded file.")

    ####################################################################################################################
    def __str__(self):
        return self.timestamp


########################################################################################################################
class JournalEntry(models.Model):
    photo = models.ManyToManyField(Photo, blank=True)
    note = models.CharField(max_length=720, blank=True, null=True, name="Note", verbose_name="Notes")
    created = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(default=timezone.now, blank=True, editable=True)


########################################################################################################################
class Meal(models.Model):
    photo = models.ManyToManyField(Photo, blank=True)
    note = models.CharField(max_length=720, blank=True, null=True, name="Note", verbose_name="Notes")
    created = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    timestamp = models.DateTimeField(default=timezone.now, blank=True, null=True, editable=True)
    ingredients = models.ManyToManyField(Ingredient)

    def __str__(self):
        return "meal"
