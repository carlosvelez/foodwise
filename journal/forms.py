########################################################################################################################
from django import forms
from django.forms import ModelForm
from django.utils import timezone
from journal.models import Meal


########################################################################################################################
class MealEntryForm(forms.Form):
    PHOTO_ROTATION_CHOICES = (
        (None, ""),
        ("left", "Left"),
        ("right", "Right"),
        ("rightside_up", "Rightside Up")
    )

    photo = forms.ImageField()
    rotation = forms.ChoiceField(choices=PHOTO_ROTATION_CHOICES)
    ingredients = forms.CharField(widget=forms.Textarea, help_text="Enter raw ingredients separated by a comma.")
    timestamp = forms.DateTimeField(widget=forms.SplitDateTimeWidget(date_format="%b %d, %Y",
                                                                     time_format="%H:%M:%S"),
                                    initial=timezone.now)
        # date_format="%b %d, %Y", time_format="%H:%M:%S")
    note = forms.CharField(widget=forms.Textarea)

    # FAVORITE_COLORS_CHOICES = (
    #     ('blue', 'Blue'),
    #     ('green', 'Green'),
    #     ('black', 'Black'),
    # )
    # favorite_colors = forms.MultipleChoiceField(required=False,
    #                                             widget=forms.CheckboxSelectMultiple,
    #                                             choices=FAVORITE_COLORS_CHOICES)
#