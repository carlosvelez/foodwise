########################################################################################################################
from django.db import models


########################################################################################################################
class Recipe(models.Model):
    name = models.CharField(name="Name", verbose_name="Names", blank=True, max_length="50")
    created = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
