GLUTEN_FREE = "gf"
FODMAPS = "fm"

FOOD_RESTRICTIONS = (
    (GLUTEN_FREE, "gluten free"),
    (FODMAPS, "fodmaps")
)
