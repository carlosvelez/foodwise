########################################################################################################################
from django.db import models
from choices import FOOD_RESTRICTIONS


########################################################################################################################
class Restrictions(models.Model):
    type = models.CharField(choices=FOOD_RESTRICTIONS, blank=True, max_length="42")
    created = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "type"


########################################################################################################################
class Ingredient(models.Model):
    name = models.CharField(max_length="42", null=True, blank=True)
    restrictions = models.ManyToManyField(Restrictions, blank=True, verbose_name="Restrictions")
    created = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "ingredient"


########################################################################################################################
class Photo(models.Model):
    file = models.ImageField(name="Photo", verbose_name="Photos", blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)

    ####################################################################################################################
    def __str__(self):
        return self.created
